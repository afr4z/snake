#pragma once
#include <ncurses.h>
#include <time.h>
#include <stdlib.h>
#include "Apple.hpp"
#include "Board.hpp"
#include "Drawable.hpp"

class SnakeGame
{
public:
	SnakeGame(int height, int width)
	{
		noecho();
		board = Board(height, width);
		board.initialize();
		game_over = false;
		srand(time(NULL));
		
	}

	void processInput()
	{
		chtype input = board.getInput();
		
	}

	void updateBoard()
	{	

        }

	void updateState()
	{
		int y, x;
		board.getEmptyCoordinates(y,x);
		board.add(Apple(y,x));

	}

	void redraw()
	{
		board.refresh();
	}

	bool isOver()
	{
		return game_over;
	}
private:
	Board board;
	bool game_over;
};
	
	
