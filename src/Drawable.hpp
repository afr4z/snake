#pragma once

class Drawable 
{
	public:
		Drawable(){
			y=x=0;
			icon=' ';
		}

		Drawable(int y, int x, chtype ch){
			      this->y = y;	
			      this->x = x;	
			      this->icon = ch;	
		}

		int getx(){
			return x;
		}

		int gety(){
			return y;
		}

		chtype getIcon(){
			return icon;
		}

	protected:
		int y,x;
		chtype icon;

};
