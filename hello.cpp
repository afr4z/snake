#include <ncurses.h>
#include "src/Drawable.hpp"
#include "src/SnakeGame.hpp"
#include "src/Board.hpp"


#define BOARD_DIM 17
#define BOARD_ROWS BOARD_DIM
#define BOARD_COLS BOARD_DIM * 2.5

int main(int argc, char **argv)
{
	initscr();
	refresh();
	
	SnakeGame game(BOARD_ROWS, BOARD_COLS);
	
	while(!game.isOver())
	{
		game.processInput();

		game.updateState();

		game.redraw();
		
	}


	getch();
	endwin();

	return 0;

}

